# mircod_tasks_2

### документация АПИ в swagger и redoc
```
http://localhost:8000/api/swagger/
http://localhost:8000/api/redoc/
```
### Запуск брокера redis
```
docker-compose up -d  --build
cd src
Запуск celery (-l info можно убрать это просто установка logging level на info чтобы видеть какие задания открылись)
celery -A website.celery worker -l info 
Запуск GUI для celery
flower -A website --port=5555
```


