from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from stdimage import StdImageField

from .managers import CustomUserManager
from .tasks import process_photo_image


def image_processor(file_name, variations, storage):
    process_photo_image.delay(file_name, variations)
    return False  # prevent default rendering


class CustomUser(AbstractUser):
    """
    Custom user model manager where email is the unique identifiers
    for authentication instead of usernames.
    """

    username = None
    email = models.EmailField(verbose_name=_("email"), unique=True)
    is_email_verified = models.BooleanField(default=False)

    avatar = StdImageField(
        upload_to="avatars",
        render_variations=image_processor,
        blank=True,
        null=True,
        variations={"thumbnail": {"width": 100, "height": 75}},
    )
    processed = models.BooleanField(default=False)  # flag that could be used for view querysets

    phone = models.CharField(verbose_name=_("phone"), max_length=20, blank=True)
    first_name = models.CharField(verbose_name=_("name"), max_length=150, blank=True)
    last_name = models.CharField(verbose_name=_("surname"), max_length=150, blank=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    def __str__(self):
        return self.email
