from rest_framework import serializers
from rest_framework.parsers import FileUploadParser

from .models import CustomUser


class AvatarSerializer(serializers.Serializer):
    parser_classes = (FileUploadParser,)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    avatar = serializers.FileField()

    class Meta:
        field = "avatar"


class UserSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = CustomUser
        fields = (
            "id",
            "email",
            "phone",
            "first_name",
            "last_name",
            "avatar",
        )

    def get_avatar(self, obj):
        return StdImageSerializer(obj, context={"request": self.context.get("request")}).data


class StdImageSerializer(serializers.Serializer):
    original = serializers.SerializerMethodField()
    thumbnail = serializers.SerializerMethodField()

    cls_map = {CustomUser: "avatar"}

    def build_url(self, field):
        if not field:
            return

        request = self.context["request"]
        host = request.get_host()
        abs_url = f"{host}{field.url}"

        if "://" not in host:
            abs_url = f"{request.scheme}://{abs_url}"
        return abs_url

    def get_original(self, obj):
        if not self.cls_map.get(obj.__class__):
            return
        field = getattr(obj, self.cls_map[obj.__class__])
        return self.build_url(field)

    def get_thumbnail(self, obj):
        if not self.cls_map.get(obj.__class__):
            return
        field = getattr(obj, self.cls_map[obj.__class__])
        return self.build_url(field.thumbnail if field else field)

    def update(self, instance, validated_data):
        return NotImplementedError

    def create(self, validated_data):
        return NotImplementedError
