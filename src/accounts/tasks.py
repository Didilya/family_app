from celery import shared_task
from django.apps import apps
from stdimage.utils import render_variations


@shared_task
def accounts_task():
    print("Hello world from accounts app")


@shared_task
def process_photo_image(file_name, variations):
    render_variations(file_name, variations, replace=True)
    obj = apps.get_model("accounts", "CustomUser").objects.get(avatar=file_name)
    obj.processed = True
    obj.save()
