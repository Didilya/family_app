from django.urls import path
from drf_yasg import openapi
from drf_yasg.generators import OpenAPISchemaGenerator
from drf_yasg.utils import swagger_auto_schema
from drf_yasg.views import get_schema_view
from rest_framework import routers
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .views import UserViewSet

app_name = "accounts"

router = routers.SimpleRouter()
router.register("user", UserViewSet, basename="user")

schema_view = get_schema_view(
    openapi.Info(title="Sharing Service API", default_version="v0.1"),
    public=True,
    generator_class=OpenAPISchemaGenerator,
    permission_classes=(IsAuthenticated,),
)


@swagger_auto_schema()
@api_view(["POST"])
def example(request):
    return Response("Hello !")


urlpatterns = [
    path("example/", example),
    path(
        "swagger/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
]

urlpatterns += router.urls
